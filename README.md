
# Paint++ is Now Open Source! 
![Paint++ Logo](https://gitlab.com/uploads/-/system/project/avatar/14920430/PaintPP.png)

We are excited to announce that **Paint++** is now officially open source! 🚀 You can access the project publicly at:
👉 **[Paint++ on GitLab](https://gitlab.com/RationalPI/paintpp)**

## Contributing 🤝

We welcome contributions from the community! Whether you're fixing bugs, adding features, or improving documentation, check out our contribution guidelines and get involved.

## Stay Connected 📢

- Report issues, request features, and collaborate with the community via **[Paint++ Issues](https://gitlab.com/RationalPI/paintpp/-/issues)**

- Join discussions and contribute to the development of Paint++!

Thank you for supporting open-source software.